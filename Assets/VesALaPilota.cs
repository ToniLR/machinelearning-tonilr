﻿using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using UnityEngine;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using UnityEngine.UI;

public class VesALaPilota : Agent
{
    bool canJump = true;
    private int score = 0;
    private int max = 0;
    public float spd = 1;
    public Text maxTxt;
    public Text scoreTxt;
    public Vector3 bolaInit;

    [SerializeField] private Transform target;
    public float Force = 10f;

    public Rigidbody rb;

    public override void Initialize()
    {
        bolaInit = this.transform.position;
        scoreTxt.text = "" + score;
        maxTxt.text = "" + max;
        rb = GetComponent<Rigidbody>();

    }
    public override void OnEpisodeBegin()
    {
       
        score = 0;
        scoreTxt.text = "" + score;
        rb = GetComponent<Rigidbody>();
        transform.localPosition = Vector3.zero;
    }
    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(transform.localPosition);
        sensor.AddObservation(target.localPosition);
 


    }
    public override void OnActionReceived(ActionBuffers actions)
    {
        float moveX = actions.ContinuousActions[0];
        float moveZ = actions.ContinuousActions[1];

        int salto = (int)Mathf.Floor(actions.ContinuousActions[2]);

        print(salto);
        
        if (salto == 1)
        {
            this.GetComponent<Rigidbody>().AddForce(new Vector3(0,2,0.5f), ForceMode.Impulse);
            //this.GetComponent<Rigidbody>().AddForce(new Vector3(0, 50,0));
          

        }



        transform.localPosition += new Vector3(moveX, 0, moveZ) * Time.deltaTime * spd;
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<float> acCont = actionsOut.ContinuousActions;
        acCont[0] = Input.GetAxisRaw("Horizontal");
        acCont[1] = Input.GetAxisRaw("Vertical");

        acCont[2] = 0;
        if (Input.GetKey(KeyCode.Space))
        {
            acCont[2] = 1;
        }
     
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "pelota")
        {
            SetReward(10f);
            print("BIG RAMBO");
            score++;
            scoreTxt.text = "" + score;
            if (score > max)
            {
                max = score;
                maxTxt.text = "" + max;
                scoreTxt.text = "" + score;
            }
            this.transform.position = bolaInit;
        }
        if (other.transform.tag == "wall")
        {
            SetReward(-7f);
            if (score > max)
            {
                max = score;
                maxTxt.text = "" + max;
            }
            EndEpisode();
        }
        if (other.transform.tag == "muerte")
        {
            SetReward(-1f);
            if (score > max)
            {
                max = score;
                maxTxt.text = "" + max;
                
            }
            EndEpisode();
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "suelo")
        {
            SetReward(5f);
       
        }
    }

  



}
